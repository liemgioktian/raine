/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50516
Source Host           : localhost:3306
Source Database       : raine

Target Server Type    : MYSQL
Target Server Version : 50516
File Encoding         : 65001

Date: 2014-03-12 16:01:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', '11', 'Ah Tan Car Services', '1 Ubi Avenue Singapore 123456. Tel:67891234, Voucher $50 off car repair', null);
INSERT INTO `product` VALUES ('2', '12', 'Business Management Software', 'PBASolutions provides easy to learn and easy to use fully integrated business management solutions that help to increase the productivity of people and processes across your organisation. Whether you are a start up business, a small or medium sized business or a large organisation, HansaWorld business productivity software solutions provide the perfect business platform to help you reach your specific productivity goals and can be tailored to your specific business needs.', null);
INSERT INTO `product` VALUES ('3', '13', 'Transportation During CNY 14 ', 'Planning to go around visiting your loved ones during CNY this year? In need of transportation for whole day..please contact me via email or text me your planning for the day itself and ill give you a qoute either based on trips on per person', null);
INSERT INTO `product` VALUES ('4', '14', 'Cheap Insurance Premium', 'We will source for you a few quotations from insurers and get the BEST possible Lowest Insurance Quotes specially for you.', null);
INSERT INTO `product` VALUES ('5', '15', 'affordable car rental', 'A few minutes of your times might help you to save on your Motor Insurance. Just complete the online form on Website: www.motorcarinsurance.com.sg or contact Agnes Tan at 90233303 for a NON OBLIGATION Free Quotation!!', null);
INSERT INTO `product` VALUES ('6', '16', 'top notch driver', 'I have many regulars clients with testimonies recommending my skills and service as PROFESSIONAL driver, and know the finest restaurants for great dining as well!', null);
INSERT INTO `product` VALUES ('7', '16', 'bn 3 in 1 bed set w 2 mattresses', 'Single sz 6\" mattresses for top n bottom\nAvailable in black,brown n cream\nAdd $30 for other colors\n\npls call Su 91912343\nw del n installation', null);
INSERT INTO `product` VALUES ('8', '16', 'u have manpower-only', 'A few minutes of your times might help you to save on your Motor Insurance. Just complete the online form on Website: ', null);
INSERT INTO `product` VALUES ('9', '16', 'bn wardrobe-warehse sales!!', '10\' truck n driver. Available immediate!!!!', null);
INSERT INTO `product` VALUES ('10', '16', 'WARDROBE @CLEMENTI', 'prefer nite time pls call Su-91912343 asap', null);
INSERT INTO `product` VALUES ('11', '16', 'preowned 2 mth old queen', 'pls call Pang 92236234 Comes with delivery n installation', null);

-- ----------------------------
-- Table structure for `service`
-- ----------------------------
DROP TABLE IF EXISTS `service`;
CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of service
-- ----------------------------
INSERT INTO `service` VALUES ('1', 'Car Services', 'car.png');
INSERT INTO `service` VALUES ('2', 'Dining', 'dining.png');
INSERT INTO `service` VALUES ('3', 'Pet Services', 'pet.png');
INSERT INTO `service` VALUES ('4', 'Lifestyle & Weliness', 'life.png');
INSERT INTO `service` VALUES ('5', 'Home Services', 'home.png');
INSERT INTO `service` VALUES ('6', 'Others', 'others.png');
INSERT INTO `service` VALUES ('7', 'Car Servicing', null);
INSERT INTO `service` VALUES ('8', 'Car Sales & Rental', null);
INSERT INTO `service` VALUES ('9', 'Car Towing & Battery Services', null);
INSERT INTO `service` VALUES ('10', 'Mobile Car Groaming / Polishing', null);
INSERT INTO `service` VALUES ('11', 'Maintenance', null);
INSERT INTO `service` VALUES ('12', 'accident claim', null);
INSERT INTO `service` VALUES ('13', 'penal beating', null);
INSERT INTO `service` VALUES ('14', 'spraying', null);
INSERT INTO `service` VALUES ('15', 'car wash', null);
INSERT INTO `service` VALUES ('16', 'car polish', null);
INSERT INTO `service` VALUES ('17', 'car repair', null);
INSERT INTO `service` VALUES ('18', 'car inspection', null);
INSERT INTO `service` VALUES ('19', 'car accessories', null);
INSERT INTO `service` VALUES ('20', 'tires', null);
INSERT INTO `service` VALUES ('21', 'battery', null);
INSERT INTO `service` VALUES ('22', 'car parts', null);
INSERT INTO `service` VALUES ('23', 'car airbrush', null);

-- ----------------------------
-- Table structure for `service_hierarchy`
-- ----------------------------
DROP TABLE IF EXISTS `service_hierarchy`;
CREATE TABLE `service_hierarchy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of service_hierarchy
-- ----------------------------
INSERT INTO `service_hierarchy` VALUES ('1', '1', '0');
INSERT INTO `service_hierarchy` VALUES ('2', '2', '0');
INSERT INTO `service_hierarchy` VALUES ('3', '3', '0');
INSERT INTO `service_hierarchy` VALUES ('4', '4', '0');
INSERT INTO `service_hierarchy` VALUES ('5', '5', '0');
INSERT INTO `service_hierarchy` VALUES ('6', '6', '0');
INSERT INTO `service_hierarchy` VALUES ('7', '7', '1');
INSERT INTO `service_hierarchy` VALUES ('8', '8', '1');
INSERT INTO `service_hierarchy` VALUES ('9', '9', '1');
INSERT INTO `service_hierarchy` VALUES ('10', '10', '1');
INSERT INTO `service_hierarchy` VALUES ('11', '11', '7');
INSERT INTO `service_hierarchy` VALUES ('12', '12', '7');
INSERT INTO `service_hierarchy` VALUES ('13', '13', '7');
INSERT INTO `service_hierarchy` VALUES ('14', '14', '7');
INSERT INTO `service_hierarchy` VALUES ('15', '15', '7');
INSERT INTO `service_hierarchy` VALUES ('16', '16', '7');
INSERT INTO `service_hierarchy` VALUES ('17', '17', '7');
INSERT INTO `service_hierarchy` VALUES ('18', '18', '7');
INSERT INTO `service_hierarchy` VALUES ('19', '19', '7');
INSERT INTO `service_hierarchy` VALUES ('20', '20', '7');
INSERT INTO `service_hierarchy` VALUES ('21', '21', '7');
INSERT INTO `service_hierarchy` VALUES ('22', '22', '7');
INSERT INTO `service_hierarchy` VALUES ('23', '23', '7');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'customer', 'e10adc3949ba59abbe56e057f20f883e');
