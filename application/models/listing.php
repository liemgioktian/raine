<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Listing extends CI_Model{
    
    protected $table = 'product';
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    function draw($childrens=array(),$node='root', $limit=6, $offset=0)
    {
        if(empty($childrens))$this->db->where("$this->table.service",$node);
        else $this->db->where_in("$this->table.service",$childrens);
        return $this->db
                ->select("$this->table.*")
                ->from($this->table)
                ->limit($limit,$offset)
                ->get()->result();
    }
    
}
