<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Service extends MY_Model{
    
    protected $table = 'service';
    protected $hierarchy = 'service_hierarchy';
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    function breadcrumbs($node=0)
    {
        $links[0] = array('href'=>  site_url(),'title'=>'Home');
        $links[1] = array('href'=>  site_url('pages/personal'),'title'=>'Personal');
        if($node==0) return $links;
        
        $getNode = $this->select(array('id'=>$node),false);
        $links[2] = array('href'=>  site_url('pages/personal/'.$node),'title'=>$getNode['service']);
        
        return $links;
    }
    
    function list_service($parent=0)
    {
        return $this->db
            ->select('service.*')
            ->from($this->table)
            ->join($this->hierarchy,"$this->table.id=$this->hierarchy.service",'left')
            ->where("$this->hierarchy.parent",$parent)
            ->get()->result();
    }
    
    function getDetail($where=null)
    {
        if(null!=$where)$this->db->where($where);
        return $this->db
            ->select('*')
            ->from($this->table)
            ->join($this->hierarchy,"$this->table.id=$this->hierarchy.service",'left')
            ->get()->result();
    }
    
}
