<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends MY_Model{
    
    public function __construct()
    {
        parent::__construct();
        $this->table = 'user';
    }
    
    function getLogin($attr=null)
    {
        if(!$this->session->userdata('id')) return false;
        else return $attr==null?
                $this->session->userdata($attr):
                $this->session->all_userdata();
    }
    
    function addUser(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        
        $data = array(
            'nim' => $nim,
            'password' => $password
        );
        $this->db->insert('user',$data);
    }
}
