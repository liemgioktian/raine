<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Model extends CI_Model {
    
    protected $table;
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function select($item = null, $list = false)
    {
        $query = ($item == null) ? $this->db->get($this->table) : $this->db->get_where($this->table, $item);
        return ($list) ? $query->result() : $query->row_array();
    }
    
    public function insert($item)
    {
        $this->db->insert($this->table,$item);
    }
    
    public function delete($item)
    {
        return $this->db->where('id', $item['id'])->delete($this->table);
    }
    
}
