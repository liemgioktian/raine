<ul class="task-list" id="breadcrumbs">
    <li>
        <a href="javascript:;">You are here :</a>
        <?php foreach($breadcrumbs as $bc): ?>
            <a href="<?= $bc['href'] ?>"><?= $bc['title'] ?></a>
            <?= $bc!=end($breadcrumbs)?'>>':'' ?>
        <?php endforeach; ?>
    </li>
</ul>