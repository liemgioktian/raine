<!--      Header      -->
<section class="content">

    <div class="content-liquid-full">
        <div class="container">

            <!-- Header Bar Start -->
            <div class="row header-bar" id="step2">

                <div class="col-sm-4">
                    <a href="<?= site_url() ?>"><img src="ui/images/logo.jpg"/></a>
                </div>

                <?php include APPPATH.'/views/parts/auth.php'; ?>
                
                <!-- Header Bar End -->
            </div>

        </div>
    </div>
</section>
