<?php foreach ($products as $item): ?>       
    <div class="col-md-4">        
        <div class="box social-stats">
            <div class="title-bar">
                <a href="javascript:;">
                    <i class="fa fa-users"></i><?= $item->title ?>
                </a>
            </div>
            <div align="center" style="height: 265px;">
                <img src="<?= base_url() ?>ui/images/logo.jpg" height="200" width="200">
                <br/>
                <?= $item->description ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>