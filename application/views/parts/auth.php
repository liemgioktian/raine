<div class="boxed no-padding col-sm-8">
    <div class="row">
        <div class="boxed no-padding col-sm-12">
            <?php if (!$isLoggedIn): ?>
                <div class="col-sm-10">
                    <ul class="right-icons">
                        <li>
                            <a href="#">Login</a>
                            <ul class="dropdown">
                                <li>
                                    <div class="inner">
                                        <form method="post"
                                              action="<?= site_url('user/login?redirect=' . str_replace(site_url(), '', current_url())) ?>"
                                              class="basic-form inline-form">
                                            <div class="col-md-4"><label for="username">Username</label></div>
                                            <div class="col-md-8"><input type="text" name="username" id="username" placeholder="username"></div>
                                            <div class="col-md-4"><label for="password">Password</label></div>
                                            <div class="col-md-8"><input type="password" name="password" id="password" placeholder="password"></div>

                                            <div class="col-md-8 col-md-offset-4">
                                                <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-check">Login</i></button>
                                            </div>

                                            <div class="clearfix"></div>
                                        </form>

                                    </div>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="#">Register</a>
                            <ul class="dropdown">
                                <li>
                                    <div class="inner">
                                        <form method="post" 
                                              action="<?= site_url('user/registration?redirect=' . str_replace(site_url(), '', current_url())) ?>" 
                                              class="basic-form inline-form">
                                            <div class="col-md-4"><label for="username">Username</label></div>
                                            <div class="col-md-8"><input type="text" name="username" id="username" placeholder="username"></div>
                                            <div class="col-md-4"><label for="password">Password</label></div>
                                            <div class="col-md-8"><input type="password" name="password" id="password" placeholder="password"></div>
                                            <div class="col-md-4"><label for="confirm">Confirm</label></div>
                                            <div class="col-md-8"><input type="password" name="confirm" id="confirm" placeholder="password"></div>

                                            <div class="col-md-8 col-md-offset-4">
                                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check">Register</i></button>
                                            </div>

                                            <div class="clearfix"></div>
                                        </form>

                                    </div>
                                </li>

                            </ul>

                        </li>
                    </ul>
                </div>
            <?php else: ?>
                <div class="pull-right" style="margin: 20px 30px 0 0">
                    Welcome, <?= $userlogin['username'] ?><br/>
                    <a href="<?= site_url('user/logout?redirect=' . str_replace(site_url(), '', current_url())) ?>">logout</a>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
