<?php
    $notification = null;
    if($this->session->flashdata('notification'))
        $notification = $this->session->flashdata('notification');
    if($notification!=null):
?>
<ul class="task-list" id="notification">
    <li>
        <span class=""><?= $notification ?></span>
        <div class="actions">
            <a href="javascript:$('ul#notification').remove()" class="delete"><i class="fa fa-times"></i></a>
        </div>
    </li>
</ul>
<?php endif; ?>