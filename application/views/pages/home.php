<!-- Row Start -->
<div class="row" style="padding-top: 10%; padding-bottom: 15%">
    <div class="col-lg-12">
    <div class="col-md-6">
        <div class="col-md-1"></div>
        <div class="col-md-11">
        <!-- Social Box - Projects -->
        <div class="social-box projects">

            <div class="icon-container">
                <div class="inner">
                    <i class="fa fa-building-o"></i>
                </div>
            </div>

            <div class="text-container">
                <div class="inner">
                    <a href="javascript:;"><span>Comercial / Business</span></a>
                </div>
            </div>
        </div>
        <!-- Social Box - Projects End -->
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-11">
        <!-- Social Box - Projects -->
        <div class="social-box posts">

            <div class="icon-container">
                <div class="inner">
                    <p><i class="fa fa-user"></i></p>
                </div>
            </div>

            <div class="text-container">
                <div class="inner">
                    <a href="<?= site_url('pages/personal') ?>"><span>Personal</span></a>
                </div>
            </div>
        </div>
        <!-- Social Box - Projects End -->
        </div>
    </div>
    </div>
</div>
<!-- Row End -->