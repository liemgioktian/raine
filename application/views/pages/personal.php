<div class="col-lg-12">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            
        <?php foreach($items as $item): ?>       
        <div class="col-md-4">        
            <div class="box social-stats">
                <div class="title-bar">
                    <a href="<?= site_url('pages/personal/'.$item->id) ?>">
                        <i class="fa fa-users"></i><?= $item->service ?>
                    </a>
                </div>
                <div align="center">
                    <img src="<?= base_url('ui/images/'.$item->image) ?>" height="200" width="200">
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        
        </div>
    </div>
</div>