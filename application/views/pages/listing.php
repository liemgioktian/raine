<div class="col-lg-12">
    <div class="row">
        <?php foreach ($items as $item): ?>
            <div class="boxed no-padding col-md-3">
                <form class="full-form">
                    <div class="inner no-radius" style="height: 120px;">
                        <div class="title-bar">
                            <h3><b><?= $item->service ?></b></h3>
                        </div>
                        <?php if (isset($sub_items[$item->id])): ?>
                        <div class="col-md-12">
                            <select name="subcategory" class="subcategory">
                                <option value="0" selected="selected">choose sub category</option>
                                <?php foreach ($sub_items[$item->id] as $sub_item): ?>
                                <option value="<?= $sub_item->id ?>"><?= $sub_item->service ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <?php endif; ?>
                    </div>
                </form>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<script type="text/javascript">
    $('.subcategory').change(function(){
        $node = $(this).val();
        loadProductList($node);
    });
    $(function(){loadProductList(7)});
    loadProductList = function($node){
        $('div#product_list').load('<?= site_url('listings/adslist') ?>/'+$node);
    }
</script>

<div class="col-lg-12" style="margin-top: 20px">
    <div class="row">
        <div class="col-md-10 col-md-offset-1" id="product_list">
            <!--product list goes here-->
        </div>
    </div>
</div>