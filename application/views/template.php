<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Dream Concierge</title>
        <base href="<?= base_url() ?>" />
        
        <!-- Template  -->
        <link rel="stylesheet" type="text/css" href="ui/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="ui/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="ui/effects/menu-effects.css">

        <!-- Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lato:400,100italic,100,300italic,300,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <!-- Assets -->
        <link rel='stylesheet' type='text/css' href='ui/jquery-ui/ui-lightness/jquery-ui-1.10.3.custom.css' />

        <!-- Theme Styles -->
        <link rel="stylesheet" type="text/css" href="ui/css/styles-lessku.css">
        <link rel="stylesheet" type="text/css" href="ui/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="ui/css/animate.css">

  <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="assets/flotcharts/excanvas.min.js"></script><![endif]-->
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ui/images/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ui/images/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ui/images/favicon.ico">
        <link rel="apple-touch-icon-precomposed" href="ui/images/favicon.ico">
        <link rel="shortcut icon" href="ui/images/favicon.ico">
        <link rel="shortcut icon" href="ui/images/favicon.ico">

        <!-- Google Maps --
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
        <!-- End Template  -->
        <!-- Javascript -->
        <script src="ui/jquery/jquery.min.js"></script>
        <script src="ui/bootstrap/js/bootstrap.min.js"></script>
        
        <!-- jQuery UI -->
        <script src="ui/jquery-ui/jquery-ui-1.10.3.custom.min.js"></script>

    </head>
    <body>
        <?php 
        
            $isLoggedIn = false;
            $userlogin = array();
            if($this->session->userdata('id')){
                $isLoggedIn = true;
                $userlogin = $this->session->all_userdata();
            }
        
        ?>
        
        <?php include APPPATH . 'views/parts/header.php'; ?>
        <?php include APPPATH . 'views/parts/notification.php'; ?>
        <?php if(current_url()!=site_url())include APPPATH . 'views/parts/breadcrumbs.php'; ?>
        <?php include APPPATH . "views/pages/$page.php"; ?>
        <?php include APPPATH . 'views/parts/footer.php'; ?>
    </body>
</html>