<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class user extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('auth');
        $this->load->helper(array('url','form'));
    }
    
    function login()
    {
        $fwd = $this->input->get('redirect');
        $post = $this->input->post();
        $post['password'] = md5($post['password']);
        $user = $this->auth->select($post,false);
        if(empty($user)) $this->session->set_flashdata('notification','login failed');
        else $this->session->set_userdata($user);
        redirect(site_url($fwd));
    }
    
    function logout()
    {
        $fwd = $this->input->get('redirect');
        $this->session->sess_destroy();
        redirect(site_url($fwd));
    }
    
    function registration(){
        $fwd = $this->input->get('redirect');
        $post = $this->input->post();
        
        if($post['password']!=$post['confirm'])
            $this->session->set_flashdata('notification','registration failed, incorrect password');
        else{
            $post['password'] = md5($post['password']);
            unset($post['confirm']);
            $this->auth->insert($post);
            $this->session->set_flashdata('notification','registration success, please login');
        }
        redirect(site_url($fwd));
    }
}
