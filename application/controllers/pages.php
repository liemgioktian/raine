<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends CI_Controller {

    public function index() {
        $pass['page'] = 'home';
        $this->load->view('template',$pass);
    }

    public function personal($root_service=0) {
        $this->load->model('service');
        $pass['page'] = $root_service==null?'personal':'listing';
        $pass['items'] = $this->service->list_service($root_service);
        if($root_service!=0){
        	foreach($pass['items'] as $parent)
        		$pass['sub_items'][$parent->id] = $this->service->list_service($parent->id);
        }
        $pass['breadcrumbs'] = $this->service->breadcrumbs($root_service);
        $this->load->view('template',$pass);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */