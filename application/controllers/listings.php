<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Listings extends CI_Controller{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('listing','service'));
    }
    
    function adslist($node='root', $limit=6, $offset=0)
    {
        $sub_node = $this->service->getDetail(array('parent'=>$node),true);
        if(empty($sub_node)) $data['products'] = $this->listing->draw(array(),$node,$limit,$offset);
        else{ 
            foreach($sub_node as $node) $nodes[] = $node->id;
            $data['products'] = $this->listing->draw($nodes,$node,$limit,$offset);
        }
        #die($this->db->last_query());
        $this->load->view('parts/products',$data);
    }
    
}
