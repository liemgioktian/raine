<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class notification {

    protected $type;
    protected $message;
    
    function set($type,$message)
    {
        $this->type = $type;
        $this->message = $message;
    }
    
    function get(){
        return array(
            'type'=> $this->type,
            'message'=> $this->message,
        );
    }
    
}
